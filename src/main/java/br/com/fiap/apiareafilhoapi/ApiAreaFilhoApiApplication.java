package br.com.fiap.apiareafilhoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiAreaFilhoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiAreaFilhoApiApplication.class, args);
	}

}
