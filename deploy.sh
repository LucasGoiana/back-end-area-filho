#!/bin/bash

docker pull registry.gitlab.com/lucasgoiana/back-end-area-filho

countContainers=$(docker ps -a | wc -l)

echo $countContainers

if [ $countContainers -gt 1 ]
then
    docker rm -f $(docker ps -a -q)
fi

docker run -d -i --name back-end-area-filho -p 8082:8082 registry.gitlab.com/lucasgoiana/back-end-area-filho



